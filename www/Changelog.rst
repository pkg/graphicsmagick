2023-01-14  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - version.sh: Updated for 1.3.40 release.

  - NEWS.txt: Updated the news.

2023-01-13  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - coders/jxl.c (ReadJXLImage): Cache and trace extra channel info.

2023-01-11  Fojtik Jaroslav  <JaFojtik@yandex.com>

  - coders/wpg.c Fixed Monochromatic bilevel WPG should answer to
    gm identify file.wpg ..... PseudoClass 2c 8-bit

2023-01-08  Fojtik Jaroslav  <JaFojtik@yandex.com>

  - coders/wpg.c Fixed deffect in WPG header reading.

2023-01-08  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - coders/png.c (WriteOnePNGImage): Use lower-case raw profile
    identifiers (e.g. 'Raw profile type xmp') because exiftool expects
    that.  Partially addresses concerns raised by SourceForge bug #682
    "Invalid storage of XMP in PNGs".

  - www/INSTALL-unix.rst: Add notes about required libjxl versions.

  - README.txt: Add notes about required libjxl versions.

2023-01-08 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/tests/runtest.bat Added new tests for WEBP, BMP2 & BMP3.
    These tests are passing.

2023-01-07  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - NEWS.txt: Updated the news.

  - It is 2023 now!  Update copyrights, rotate changelogs, etc.

  - magick/blob.c (OpenBlob): Zlib has never supported opening Unix
    'compress' .Z files (although gzip does).  So don't open such
    files using zlib.

  - coders/sun.c: Add IM1, IM8, and IM24 magick aliases for Sun
    Raster format since those are the historically correct extensions.

2023-01-06  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - coders/sun.c (ReadSUNImage): Address oss-fuzz 54810
    "graphicsmagick:coder\_SUN\_fuzzer: Heap-buffer-overflow in
    ReadSUNImage".

  - coders/pict.c (WritePICTImage): Fix use of logical operator
    where binary operator is needed.

2023-01-05 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/installer/inc/body.isx 64 bit distribution MUST NOT
    be installed on pure 32 bit system. Sanity check added.

2023-01-05 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/installer/inc/body.isx
  - VisualMagick/installer/inc/files-dlls.isx
    (VisualMagick/installer/redist/VC2008SP1/vcredist\_x64.exe must be downloaded from www).
    (VisualMagick/installer/redist/VC2008SP1/vcredist\_x86.exe must be downloaded from www).
    Fix graphics magick installer for Windows.

2023-01-04 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/tests/runtest.bat Added new tests for PGX (jp2),
    MAT, uncommented test for EPDF and PICON.

2023-01-03 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/jp2/src/appl/UTILITY.txt removed fuzz.c.

2023-01-03 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - VisualMagick/jp2/src/libjasper/pgx/LIBRARY.txt
  - jp2/src/libjasper/include/jasper/jas\_config.h
    PGX codec was not compilled into gm, now added.

2023-01-02  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - coders/pict.c: Add more tracing.

2023-01-01  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - coders/pcd.c (WritePCDTile): Handle writing image with a
    dimension of 1.

2023-01-02 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - jp2/\* Update lib jasper to 2.0.33. Code cleanly compilles, but
    there is still some problem. Will be solved later.
    jp2/src/lib/jasper/include/jasper/stdbool2.h No longer needed.

2023-01-01  Bob Friesenhahn  <bfriesen@simple.dallas.tx.us>

  - magick/utility.c (GetMagickGeometry): Assure that width and
    height are not scaled down to zero since it is an invalid value.

  - coders/sun.c (ReadSUNImage): Enlarge RLE output buffer in order
    to avoid buffer overflow.  Addresses oss-fuzz 54716
    "graphicsmagick:coder\_RAS\_fuzzer: Heap-buffer-overflow in
    ReadSUNImage", which is due to a new problem added since the
    1.3.39 release.

2023-01-01 Fojtik Jaroslav  <JaFojtik@yandex.com>

  - jp2/\* Update lib jasper to 2.0.0.
